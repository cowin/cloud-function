require('dotenv').config()
const admin = require('firebase-admin')
const cors = require('cors')({ origin: true })
const functions = require('firebase-functions')

const TOPIC = 'checkSessions'

const MESSAGE = {
  data: {
    title: TOPIC,
    body: 'Please check if new vaccince slots are open.',
  },
  topic: TOPIC,
}

admin.initializeApp()

exports.checksessions = functions.https.onRequest((req, res) =>
  cors(req, res, async () => {
    try {
      if (req.headers.authorization === functions.config().cowinjb.key) {
        const response = await admin.messaging().send(MESSAGE)
        console.log('Successfully sent message:', response)
        res.send({ notice: 'Successfully sent message', response })
      } else {
        res.status(401).send()
      }
    } catch (error) {
      res.status(400).send(error)
    }
  })
)

exports.subscribe = functions.https.onRequest((req, res) =>
  cors(req, res, async () => {
    try {
      const response = await admin.messaging().subscribeToTopic(req.body, TOPIC)
      res.json({ notice: 'Successfully subscribed to topic.', response })
    } catch (error) {
      res.status(400).send(error)
    }
  })
)
